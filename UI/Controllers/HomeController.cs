﻿using System.Collections.Generic;
using System.Web.Mvc;
using Global.Models.ViewModels;
using UI.Repository.Product;

namespace UI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProductService _productService;

        public HomeController(IProductService productService)
        {
            _productService = productService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetProducts(string key = null)
        {
            var model = _productService.GetProducts();
            return Json(new { response = model }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProductDetail(int id = 0)
        {
            var model = new ProductViewModel();

            if(id!= 0)
                model = _productService.GetProductById(id);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddOrUpdateProduct(ProductViewModel data)
        {
            var result = _productService.AddOrUpdateProduct(data);
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult RemoveProduct(string id)
        {
            var result = _productService.DeleteProduct(int.Parse(id));
            return Json(result);
        }

    }
}