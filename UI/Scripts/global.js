﻿$(function () {
    ViewHelper = new SuitSupplyViewHelper();
    ViewHelper.init();

});

var SuitSupplyViewHelper = function () {
    var self = this;
    
    self.overlay = null;
    self.loading = null;

    self.init = function () {
        self.overlay = $("#overlay");
        self.loading = $("#loading");
    };

    self.showOverlay = function () {
        
        self.overlay.show();

    };

    self.hideOverlay = function () {
        self.overlay.hide();
    };

    self.showLoading = function () {

        self.loading.show();
    };

    self.hideLoading = function () {

        self.loading.hide();

    };
};


function AddAntiForgeryToken(data, optionalParentDom) {
    var token = "";

    if (optionalParentDom) {
        token = optionalParentDom.find("input[name=__RequestVerificationToken]").val();
    } else {
        token = $('input[name=__RequestVerificationToken]').val();
    }

    data.__RequestVerificationToken = token;

    return data;
}

Handlebars.registerHelper('hrefHandler', function (url) {
    if (ValidationHelper.IsNullOrEmptyString(url))
        return " ";

    return new Handlebars.SafeString("href=\"" + url + "\"");
});
