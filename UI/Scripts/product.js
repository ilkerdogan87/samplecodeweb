﻿$(function () {
    ProductViewHelper = new SuitSupplyProductViewHelper();
    ProductViewHelper.init();

});

var SuitSupplyProductViewHelper = function () {
    var self = this;
    var searchKey = "";
    var productTable = $("#productTable");
    var templeate = Handlebars.compile($("#template-product-table").html());
    var timeout;
    var txtSearch = $("#txtSearch");
    var tBody;

    self.init = function () {
        tBody = $(".tbody");
        self.getData();

        txtSearch.keyup(function (e) {
            searchKey = txtSearch.val().trim();
            self.getData();
        });

        tBody.on("click", "a.deleteProduct", function () {
            ViewHelper.showLoading();
            var that = $(this);
            var id = that.attr("data-id");

            $.post("/home/removeProduct", AddAntiForgeryToken({ id: id }), function (response) {
                if (response) 
                    that.parents(".item").remove()
                else
                    alert("error");

                ViewHelper.hideLoading();
            });

        });

        tBody.on("click", "a.addProduct", function () {
            ViewHelper.showLoading();
            var that = $(this);
            var id = that.attr("data-id");

            $.post("/home/AddOrUpdateProduct", AddAntiForgeryToken({ id: id }), function (response) {
                if (response)
                    that.parents(".item").remove()
                else
                    alert("error");

                ViewHelper.hideLoading();
            });

        });
    };

    self.getData = function () {
        ViewHelper.showLoading();
        tBody.html("");

        $.ajax({
            type: "GET",
            url: "/home/getProducts",
            data: { key: searchKey }
        }).done(function (data) {
            var html = templeate({ products: data.response });
            tBody.append(html);

            showMessage(data.response.length);

            ViewHelper.hideLoading();
        });
    }

    function showMessage(count) {
        var messageArea = $(".noDataMessage");
        if (count<1)
            messageArea.css("display", "block")
        else
            messageArea.css("display", "none")
    }


};

