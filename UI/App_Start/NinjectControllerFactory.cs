﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using Ninject.Modules;
using UI.Repository.Product;
using UI.Repository.Token;

namespace UI.App_Start
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private readonly IKernel kernel;

        public NinjectControllerFactory()
        {
            kernel = new StandardKernel(new NinjectBindingModule());
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)kernel.Get(controllerType);
        }
    }

    public class NinjectBindingModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ITokenService>().To<TokenService>();
            Kernel.Bind<IProductService>().To<ProductService>();
        }
    }
}