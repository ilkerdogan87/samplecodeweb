﻿using System.Collections.Generic;
using Global.Models.ViewModels;

namespace UI.Repository.Product
{
    public interface IProductService
    {
        List<ProductViewModel> GetProducts(bool isOnlyActive = false, string key=null);
        ProductViewModel GetProductById(int id);
        bool AddOrUpdateProduct(ProductViewModel item);
        bool DeleteProduct(int id);
    }
}