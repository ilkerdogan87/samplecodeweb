﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;

namespace UI.Repository.Token
{
    public class TokenService : ITokenService
    {
        public string CreateToken()
        {
            var client = new RestClient("http://localhost:22415/api/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("platform", "\"Web\"");
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\r\n  \"userName\": \"sampleCodeUserName1\",\r\n  \"password\": \"{rdt21C0]=5ks\"\r\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return null;
        }
    }
}