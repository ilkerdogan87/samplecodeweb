﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Global.Helpers;
using RestSharp;

namespace UI.Helpers
{
    public class RestSharpHelper
    {
        private RestClient _client;

        public RestSharpHelper(string serviceUrl)
        {
            _client = new RestClient(ConfigHelper.ApiAddress+"/"+serviceUrl);
        }
    }
}