﻿using Newtonsoft.Json;

namespace Global.Models.ServiceModels
{
    public class BaseModel<T>
    {
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "errorMessage")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "errorNumber")]
        public string ErrorNumber { get; set; }

        [JsonProperty(PropertyName = "result")]
        public T Result { get; set; }
    }
}
