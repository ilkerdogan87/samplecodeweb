﻿using System.Configuration;

namespace Global.Helpers
{
    public class ConfigHelper
    {
        private static string GetKey(string key)
        {
            var setting = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrEmpty(setting))
                return "";
            return setting;
        }

        private static string _apiUserName;
        public static string ApiUserName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_apiUserName) == false)
                    return _apiUserName;

                _apiUserName = GetKey("ApiUserName");
                return _apiUserName;
            }
        }

        private static string _apiPassword;
        public static string ApiPassword
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_apiPassword) == false)
                    return _apiPassword;

                _apiPassword = GetKey("ApiPassword");
                return _apiPassword;
            }
        }

        private static string _apiAddress;
        public static string ApiAddress
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_apiAddress) == false)
                    return _apiAddress;

                _apiAddress = GetKey("ApiAddress");
                return _apiAddress;
            }
        }

        
    }
}
