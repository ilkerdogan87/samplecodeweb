﻿using Newtonsoft.Json;

namespace Global.Helpers
{
    public static class GeneralFunctions
    {
        public static string ToJSON(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
